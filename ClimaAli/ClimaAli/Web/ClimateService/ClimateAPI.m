//
//  ClimateAPI.m
//  ClimaAli
//
//  Created by Sferea Book on 09/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//

#import "ClimateAPI.h"
#import "WebRequest.h"
#import "WebManager.h"

static const NSString* CLIMATE_API_KEY = @"301de909bdc4ffabe79707efa101d6cf";

@implementation ClimateAPI

+(ClimateDTO*)makeClimateRequest:(NSString*)lon latitude:(NSString*)lat
{
    if([WebManager hasNetwork])
    {
        NSString* request = [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?lat=%@&lon=%@&appid=%@&lang=es&units=metric",lat,lon,CLIMATE_API_KEY];
        
        
        NSData *response = [WebRequest sendSynchronousGetFor:request WithParameters:nil];
        
        if(response && ![response isEqual:[NSNull null]] && response.length>0)
        {
            NSDictionary * responseDictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
            
            if(!responseDictionary || responseDictionary.count<=0)
            {
                return nil;
            }
            return [ClimateDTO parseFromDictionary:responseDictionary];
        }
        return nil;
    }
    else
    {
        ClimateDTO* dummyData = [[ClimateDTO alloc] init];
        dummyData.temperature = @"Temperature";
        dummyData.iconURL = @"Icon";
        dummyData.dateUpdate = [NSDate date];
        return  dummyData;
    }
}

@end
