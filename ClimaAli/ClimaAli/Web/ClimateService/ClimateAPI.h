//
//  ClimateAPI.h
//  ClimaAli
//
//  Created by Sferea Book on 09/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ClimateDTO.h"

@interface ClimateAPI : NSObject

+(ClimateDTO*)makeClimateRequest:(NSString*)lon latitude:(NSString*)lat;

@end
