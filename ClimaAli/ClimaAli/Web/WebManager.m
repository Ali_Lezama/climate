//
//  WebManager.m
//  ClimaAli
//
//  Created by Sferea Book on 09/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//

#import "WebManager.h"
#import "Reachability.h"

@implementation WebManager

+(BOOL)hasNetwork
{
    BOOL result = false;
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus] != NotReachable)
    {
        result = true;
    }
    return result;
}

@end
