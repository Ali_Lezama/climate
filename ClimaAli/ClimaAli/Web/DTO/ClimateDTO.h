//
//  ClimateDTO.h
//  ClimaAli
//
//  Created by Sferea Book on 09/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClimateDTO : NSObject

@property (strong,nonatomic) NSString* temperature;
@property (strong,nonatomic) NSString* iconURL;
@property (strong,nonatomic) NSDate* dateUpdate;

+(ClimateDTO*)parseFromDictionary:(NSDictionary*)data;

@end
