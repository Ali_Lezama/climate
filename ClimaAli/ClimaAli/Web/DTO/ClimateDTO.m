//
//  ClimateDTO.m
//  ClimaAli
//
//  Created by Sferea Book on 09/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//

#import "ClimateDTO.h"

@implementation ClimateDTO

+(ClimateDTO*)parseFromDictionary:(NSDictionary*)data
{
    if(!data)
    {
        return nil;
    }
    
    ClimateDTO* climate = [[ClimateDTO alloc] init];
    
    climate.temperature = [NSString stringWithFormat:@"%dC",(int)[[[data objectForKey:@"main"] objectForKey:@"temp"] integerValue]];
    climate.iconURL = [NSString stringWithFormat:@"http://openweathermap.org/img/w/%@.png",[(NSDictionary*)(NSArray*)[data objectForKey:@"weather"][0] objectForKey:@"icon"]];
    climate.dateUpdate = [NSDate date];
    
    return climate;
}

@end
