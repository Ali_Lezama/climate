//
//  WebManager.h
//  ClimaAli
//
//  Created by Sferea Book on 09/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebManager : NSObject

+(BOOL)hasNetwork;

@end
