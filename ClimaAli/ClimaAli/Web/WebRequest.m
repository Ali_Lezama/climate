

#import "WebRequest.h"

@implementation WebRequest

+(NSString *) getURLWithParams:(NSString*)urlOrigin andParameters:(NSString *) params
{
    if(!urlOrigin)
    {
        return nil;
    }
    
    NSMutableString * requestURL = [NSMutableString stringWithFormat: @"%@",urlOrigin];
    
    if(params)
    {
        [requestURL appendFormat:@"%@", params];
    }
    
    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
    return [requestURL stringByAddingPercentEncodingWithAllowedCharacters:set];
}

+(NSData*) sendSynchronousGetFor:(NSString*) customUrl WithParameters:(NSString *) params
{
	NSString * requestURL = [WebRequest getURLWithParams:customUrl andParameters:params];
	
	if(!requestURL)
    {
        return nil;
    }
    NSLog(@"Request:%@",requestURL);
    
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:75];
	[request setHTTPMethod: @"GET"];
	
	NSError *requestError;
	NSURLResponse *urlResponse = nil;
	NSData *response =[NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
	
	if(requestError)
    {
        NSLog(@"Error en petición: %@", requestError);
    }
    NSLog(@"Response:%@", [[NSString alloc]initWithData:response encoding:NSUTF8StringEncoding]);
	return response;
}


@end
