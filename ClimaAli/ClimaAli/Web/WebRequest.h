#import <Foundation/Foundation.h>

@interface WebRequest : NSObject

+(NSString*)getURLWithParams:(NSString*)urlOrigin andParameters:(NSString*)params;
+(NSData*)sendSynchronousGetFor:(NSString*)customUrl WithParameters:(NSString*)params;

@end
