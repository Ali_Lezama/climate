//
//  CityRegistration.h
//  ClimaAli
//
//  Created by Sferea Book on 09/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface CityRegistration : UIViewController

@property (weak,nonatomic) ViewController* previousView;

@property (weak, nonatomic) IBOutlet UIView *background;
@property (weak, nonatomic) IBOutlet UILabel *cityTitle;
@property (weak, nonatomic) IBOutlet UITextField *cityInput;
@property (weak, nonatomic) IBOutlet UILabel *countryTitle;
@property (weak, nonatomic) IBOutlet UITextField *countryInput;
@property (weak, nonatomic) IBOutlet UILabel *latitudeTitle;
@property (weak, nonatomic) IBOutlet UITextField *latitudeInput;
@property (weak, nonatomic) IBOutlet UILabel *longitudeTitle;
@property (weak, nonatomic) IBOutlet UITextField *longitudeInput;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;


@end
