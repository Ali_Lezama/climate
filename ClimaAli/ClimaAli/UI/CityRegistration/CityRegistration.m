//
//  CityRegistration.m
//  ClimaAli
//
//  Created by Sferea Book on 09/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//

#import "CityRegistration.h"
#import "CityData.h"

@interface CityRegistration ()

@end

@implementation CityRegistration

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self adjustUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)onCancel:(id)sender
{
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)onAccept:(id)sender
{
    if(![_cityInput.text isEqualToString:@""] &&
       ![_countryInput.text isEqualToString:@""] &&
       ![_latitudeInput.text isEqualToString:@""] &&
       ![_longitudeInput.text isEqualToString:@""])
    {
        CityData* data = [[CityData alloc] initWithData:_cityInput.text country:_countryInput.text longitude:_longitudeInput.text latitude:_latitudeInput.text];
        [_previousView registerNewCity:data];
    }
    
    [self dismissViewControllerAnimated:true completion:nil];
}

-(void)adjustUI
{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGFloat wid = screenBound.size.width;
    CGFloat hei = screenBound.size.height;
    self.background.bounds = CGRectMake(0,0,wid*0.75f,hei*0.55f);
    self.background.center = CGPointMake(wid*0.5f,hei*0.5f);
    self.background.layer.cornerRadius = 15;
    self.background.layer.masksToBounds = NO;
    self.background.layer.shadowRadius = 1.0;
    self.background.layer.shadowColor = [UIColor colorWithRed:129/255.0f green:207/255.0f blue:224/255.0f alpha:1.0].CGColor;
    self.background.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    self.background.layer.shadowOpacity = 1.0;
    
    self.cancelButton.bounds = CGRectMake(0,0,wid*0.2f,hei*0.07f);
    self.cancelButton.center = CGPointMake(wid*0.15f,hei*0.5f);
    
    self.acceptButton.bounds = CGRectMake(0,0,wid*0.2f,hei*0.07f);
    self.acceptButton.center = CGPointMake(wid*0.6f,hei*0.5f);
    
    self.cityTitle.bounds = CGRectMake(0,0,wid*0.6f,hei*0.07f);
    self.cityTitle.center = CGPointMake(wid*0.36f,hei*0.05f);
    self.cityInput.bounds = CGRectMake(0,0,wid*0.6f,hei*0.05f);
    self.cityInput.center = CGPointMake(wid*0.35f,hei*0.09f);
    self.cityInput.layer.shadowRadius = 1.0;
    self.cityInput.layer.shadowColor = [UIColor colorWithRed:129/255.0f green:207/255.0f blue:224/255.0f alpha:1.0].CGColor;
    self.cityInput.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    self.cityInput.layer.shadowOpacity = 1.0;
    
    self.countryTitle.bounds = CGRectMake(0,0,wid*0.6f,hei*0.07f);
    self.countryTitle.center = CGPointMake(wid*0.36f,hei*0.15f);
    self.countryInput.bounds = CGRectMake(0,0,wid*0.6f,hei*0.05f);
    self.countryInput.center = CGPointMake(wid*0.35f,hei*0.19f);
    self.countryInput.layer.shadowRadius = 1.0;
    self.countryInput.layer.shadowColor = [UIColor colorWithRed:129/255.0f green:207/255.0f blue:224/255.0f alpha:1.0].CGColor;
    self.countryInput.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    self.countryInput.layer.shadowOpacity = 1.0;
    
    self.latitudeTitle.bounds = CGRectMake(0,0,wid*0.6f,hei*0.07f);
    self.latitudeTitle.center = CGPointMake(wid*0.36f,hei*0.25f);
    self.latitudeInput.bounds = CGRectMake(0,0,wid*0.6f,hei*0.05f);
    self.latitudeInput.center = CGPointMake(wid*0.35f,hei*0.29f);
    self.latitudeInput.layer.shadowRadius = 1.0;
    self.latitudeInput.layer.shadowColor = [UIColor colorWithRed:129/255.0f green:207/255.0f blue:224/255.0f alpha:1.0].CGColor;
    self.latitudeInput.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    self.latitudeInput.layer.shadowOpacity = 1.0;
    
    self.longitudeTitle.bounds = CGRectMake(0,0,wid*0.6f,hei*0.07f);
    self.longitudeTitle.center = CGPointMake(wid*0.36f,hei*0.35f);
    self.longitudeInput.bounds = CGRectMake(0,0,wid*0.6f,hei*0.05f);
    self.longitudeInput.center = CGPointMake(wid*0.35f,hei*0.39f);
    self.longitudeInput.layer.shadowRadius = 1.0;
    self.longitudeInput.layer.shadowColor = [UIColor colorWithRed:129/255.0f green:207/255.0f blue:224/255.0f alpha:1.0].CGColor;
    self.longitudeInput.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    self.longitudeInput.layer.shadowOpacity = 1.0;
}

@end
