//
//  RouletteController.h
//  RouletteComponent
//
//  Created by Sferea on 05/01/18.
//  Copyright © 2018 Sferea. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SFRouletteControllerBase.h"
#import "SFRouletteBase.h"

@interface SFImagesRouletteController : SFRouletteControllerBase

-(id)init:(SFRouletteBase*)toControl;
-(void)adjustRoulette:(float)angleToAdjust withAnimation:(BOOL)withAnim;

@end
