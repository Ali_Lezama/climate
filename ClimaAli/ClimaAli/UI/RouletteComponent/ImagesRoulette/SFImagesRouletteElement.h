//
//  RouletteElement.h
//  RouletteComponent
//
//  Created by Sferea on 05/01/18.
//  Copyright © 2018 Sferea. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void(^CandidateSelectedAnimCallback)(void);

@interface SFImagesRouletteElement : NSObject
{
    @protected
    CandidateSelectedAnimCallback selectedAnimationCallback;
}

@property (nullable, nonatomic) NSString* cityValue;

-(id)init:(UIImageView*)optionImage candidateImg:(UIImageView*)candidateImage cityName:(NSString*)city;
-(UIImageView*)getOptionImage;
-(UIImageView*)getCandidateImage;
-(float)getRotation;
-(float)getWidth;
-(float)getHeight;
-(void)setScale:(float)rouletteHeight elementPercent:(float)roulettePercent;
-(void)setPosition:(float)xPosition yPosition:(float)yPosition;
-(void)setRotation:(float)newRotation angleRange:(float)range;
-(BOOL)isAngleInRange:(float)angle;
-(void)setAsCandidate;
-(void)hideCandidate;
-(void)candidateSelected:(void(^)(void))onCompleteCallBack;
-(void)clearElement;
-(void)setData:(NSString*)country latitude:(NSString*)lat longitude:(NSString*)lon temperature:(NSString*)temp;
@end
