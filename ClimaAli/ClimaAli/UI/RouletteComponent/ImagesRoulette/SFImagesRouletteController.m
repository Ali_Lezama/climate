//
//  RouletteController.m
//  RouletteComponent
//
//  Created by Sferea on 05/01/18.
//  Copyright © 2018 Sferea. All rights reserved.
//k

#import "SFImagesRouletteController.h"
#import "SFImagesRouletteManager.h"

static const float SPIN_DURATION = 0.5f;
static const float SWIPE_DURATION = 1.7f;

@interface SFRouletteControllerBase()
-(BOOL)isTapGesture:(float)distance deltaTime:(float)time;
-(void)onTapDetected:(CGPoint)position;
-(BOOL)isSwipeGesture:(float)distance deltaTime:(float)time;
@end

/**
 Clase que se encarga de controlar la ruleta con elementos de Imagen
 */
@implementation SFImagesRouletteController
{
    SFImagesRouletteManager* imagesRoulette;
}

/**
 Constructor base que desactiva los limites de rotacion
 
 @param toControl Ruleta que va a controlar este controlador
 */
-(id)init:(SFImagesRouletteManager*)toControl
{
    self = [super init:toControl];
    if(self != nil)
    {
        imagesRoulette = toControl;
        rotationStep = 1.5f;
        useRotationLimits = true;
    }
    return self;
}

/**
 Funcion que ajusta la ruleta con una animacion al angulo que se envia
 
 @param angleToAdjust Angulo al que se ajustara la ruleta
 */
-(void)adjustRoulette:(float)angleToAdjust withAnimation:(BOOL)withAnim
{
    if(allowInput)
    {
        imageAngle = atan2([roulette getImage].transform.b,[roulette getImage].transform.a);
        imageAngle = [SFImagesRouletteController toDegrees:imageAngle];
        angleToAdjust = [SFImagesRouletteController toRadians:(angleToAdjust-imageAngle)];
        if(withAnim)
        {
            [UIView animateWithDuration:SPIN_DURATION animations:^{
                [roulette getImage].transform = CGAffineTransformRotate([roulette getImage].transform,angleToAdjust);
            }];
        }
        else
        {
            [roulette getImage].transform = CGAffineTransformRotate([roulette getImage].transform,angleToAdjust);
        }
    }
}

/**
 Funcion que maneja el fin del evento touch
 
 @param position La posicion en la que termino el evento touch
 @param previousTouch La posicion previa del evento touch
 @param deltaTime El tiempo que duro el evento touch
 */
-(void)onTouchEnded:(CGPoint)position initPos:(CGPoint)previousTouch timeStamp:(float)deltaTime
{
    if(allowInput)
    {
        float deltaX = position.x - previousTouch.x;
        float deltaY = position.y - previousTouch.y;
        float squareMagnitude = (deltaX*deltaX) + (deltaY*deltaY);
        if([super isTapGesture:squareMagnitude deltaTime:deltaTime])
        {
            [super onTapDetected:position];
        }
        else if([super isSwipeGesture:squareMagnitude deltaTime:deltaTime])
        {
            if(previousTouch.y < position.y)
            {
                [imagesRoulette advanceCandidate:-1 withAnimation:true];
            }
            else
            {
                [imagesRoulette advanceCandidate:1 withAnimation:true];
            }
            [imagesRoulette updateValue];
        }
        else
        {
            [imagesRoulette makeItCandidate:true];
            [imagesRoulette updateValue];
        }
    }
}

/**
 Funcion que se encarga de rotar la ruleta con un evento swipeDown hacia un angulo dado
 
 @param angle Angulo al que rotara la ruleta con el swipe down
 */
-(void)rotateToSwipeDown:(float)angle
{
    imageAngle = atan2([roulette getImage].transform.b,[roulette getImage].transform.a);
    imageAngle = [SFRouletteControllerBase toDegrees:imageAngle];
    float difference = angle-imageAngle;
    if(difference < 0)
    {
        angle = [SFImagesRouletteController toRadians:-180];
        [UIView animateWithDuration:(SWIPE_DURATION*0.5f) animations:^
        {
            [roulette getImage].transform = CGAffineTransformRotate([roulette getImage].transform,angle);
        } completion:^(BOOL finished)
        {
            [UIView animateWithDuration:(SWIPE_DURATION*0.5f) animations:^
            {
                float missing = [SFImagesRouletteController toRadians:180+difference];
                [roulette getImage].transform = CGAffineTransformRotate([roulette getImage].transform,missing);
            } completion:^(BOOL finished)
            {
                [imagesRoulette makeItCandidate:true];
            }];
        }];
    }
    else
    {
        angle = [SFImagesRouletteController toRadians:difference];
        [UIView animateWithDuration:SWIPE_DURATION animations:^
        {
            [roulette getImage].transform = CGAffineTransformRotate([roulette getImage].transform,angle);
        }];
    }
}

/**
 Funcion que se encarga de rotar la ruleta con un evento swipeUp hacia un angulo dado
 
 @param angle Angulo al que rotara la ruleta con el swipe up
 */
-(void)rotateToSwipeUp:(float)angle
{
    imageAngle = atan2([roulette getImage].transform.b,[roulette getImage].transform.a);
    imageAngle = [SFImagesRouletteController toDegrees:imageAngle];
    angle = [SFImagesRouletteController reflectAngle:angle];
    float difference = [SFImagesRouletteController adjustAngleTo360Measure:imageAngle]-angle;
    if(difference > 180)
    {
        angle = [SFImagesRouletteController toRadians:180];
        [UIView animateWithDuration:(SWIPE_DURATION*0.5f) animations:^
        {
            [roulette getImage].transform = CGAffineTransformRotate([roulette getImage].transform,angle);
        } completion:^(BOOL finished)
        {
            [UIView animateWithDuration:(SWIPE_DURATION*0.5f) animations:^{
                float missing = [SFImagesRouletteController toRadians:-(difference-180)];
                [roulette getImage].transform = CGAffineTransformRotate([roulette getImage].transform,missing);
            } completion:^(BOOL finished)
             {
                 [imagesRoulette makeItCandidate:true];
             }];
        }];
    }
    else
    {
        angle = [SFImagesRouletteController toRadians:(angle-imageAngle)];
        [UIView animateWithDuration:SWIPE_DURATION animations:^
        {
            [roulette getImage].transform = CGAffineTransformRotate([roulette getImage].transform,angle);
        }];
    }
}
@end
