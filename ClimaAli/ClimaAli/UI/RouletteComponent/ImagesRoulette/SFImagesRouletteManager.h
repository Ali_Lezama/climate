//
//  RouletteManager.h
//  RouletteComponent
//
//  Created by Sferea on 05/01/18.
//  Copyright © 2018 Sferea. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "SFRouletteBase.h"
#import "SFImagesRouletteElement.h"

@interface SFImagesRouletteManager : SFRouletteBase

-(id)init;
-(void)assignRouletteContent:(NSMutableArray<SFImagesRouletteElement*>*)content;
-(void)makeItCandidate:(BOOL)animated;
-(void)setCandidate:(BOOL)animated;
-(void)updateValue;
-(void)advanceCandidate:(int)elementsToAdvance withAnimation:(BOOL)animated;
-(float)goToFirstCandidate:(BOOL)animated;
-(float)goToLastCandidate:(BOOL)animated;
-(void)releaseMemory;
-(void)clearRoulette;

-(void)moveSelection;

@end
