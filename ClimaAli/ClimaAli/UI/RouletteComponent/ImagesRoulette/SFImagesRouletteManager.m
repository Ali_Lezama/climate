//
//  RouletteManager.m
//  RouletteComponent
//
//  Created by Sferea on 05/01/18.
//  Copyright © 2018 Sferea. All rights reserved.
//

#import "SFImagesRouletteManager.h"
#import "SFRouletteControllerBase.h"
#import "SFImagesRouletteController.h"

static const BOOL CREATE_USING_MARGIN = false;
static const float ROULETTE_ELEMENTS_SIZE_PERCENT = 0.25f;
static const float DIAMETER_PERCENT_FOR_RADIUS = 0.353f;
static const float VALUE_FOR_SELECTION_X_CENTER = 0.75f;
static const float VALUE_FOR_SELECTION_Y_CENTER = 0.4f;
static const float VALUE_FOR_SELECTION_SIZE = 0.45f;

@interface SFRouletteBase()
-(void)adjustRouletteToConfiguration;
-(BOOL)didSelectElement:(CGPoint)toEvaluate areaCenter:(CGPoint)areaPosition;
@end

/**
 Clase que administra la ruleta de imagenes
 */
@implementation SFImagesRouletteManager
{
    SFImagesRouletteController* imagesController;
    SFImagesRouletteElement* currentCandidate;
    NSMutableArray<SFImagesRouletteElement*>* rouletteElements;
}

/**
 Constructor base que ajusta el area de seleccion e inicia la variable que almacena las imagenes
 */
-(id)init
{
    self = [super init];
    if(self != nil)
    {
        screenWidthPercentForSelectionWidth = VALUE_FOR_SELECTION_SIZE;
        rouletteElements = [[NSMutableArray<SFImagesRouletteElement*> alloc] init];
        controller = [[SFImagesRouletteController alloc] init:self];
        imagesController = [[SFImagesRouletteController alloc] init:self];
    }
    return self;
}

/**
 Funcion que muestra la ruleta en la pantalla
 */
-(void)showRoulette:(int)currentPosition
{
    [super showRoulette:currentPosition];
    for(int i = 0;i < rouletteElements.count;i++)
    {
        [rouletteElements[i] getOptionImage].hidden = false;
        [rouletteElements[i] getCandidateImage].hidden = false;
    }
    if(currentPosition != 0)
    {
        [self goToFirstCandidate:false];
        [self advanceCandidate:(currentPosition-1) withAnimation:false];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self setCandidate:true];
            [self updateValue];
        });
    }
    else
    {
        [self removeCandidate];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self makeItCandidate:true];
            [self updateValue];
        });
    }
}

/**
 Funcion que esconde la ruleta en la pantalla
 */
-(void)hideRoulette
{
    [super hideRoulette];
    for(int i = 0;i < rouletteElements.count;i++)
    {
        [rouletteElements[i] getOptionImage].hidden = false;
        [rouletteElements[i] getCandidateImage].hidden = false;
    }
}

/**
 Funcion que agrega y posiciona el contenido de la ruleta
 
 @param content Imagenes que contendra la ruleta
 */
-(void)assignRouletteContent:(NSMutableArray<SFImagesRouletteElement *> *)content
{
    [self clearRoulette];
    [self addElements:content];
    double angleForImageSize = [self getElementsAngleToPositionate];
    [super adjustRouletteToConfiguration];
    [self settleOptions:angleForImageSize];
}

/**
 Funcion que remueve los elementos actuales de la ruleta
 */
-(void)clearRoulette
{
    for(int i = 0;i < rouletteElements.count;i++)
    {
        [[rouletteElements[i] getOptionImage] removeFromSuperview];
        [[rouletteElements[i] getCandidateImage] removeFromSuperview];
    }
    [rouletteElements removeAllObjects];
}

/**
Funcion que agrega elementos a la ruleta
 
 @param elements Los elementos que se agregaran en la ruleta
 */
-(void)addElements:(NSMutableArray<SFImagesRouletteElement *> *)elements
{
    for(int i = 0;i < elements.count;i++)
    {
        [rouletteElements addObject:elements[i]];
        [rouletteImage addSubview:[elements[i] getOptionImage]];
        [rouletteImage addSubview:[elements[i] getCandidateImage]];
        [rouletteImage bringSubviewToFront:[elements[i] getOptionImage]];
        [rouletteElements[i] setScale:rouletteRect.size.height elementPercent:ROULETTE_ELEMENTS_SIZE_PERCENT];
    }
}

/**
 Funcion que regresa el angulo al que se iran colocando los elementos de la ruleta
 
 @return Angulo al que se colocaran los elementos de la ruleta
 */
-(double)getElementsAngleToPositionate
{
    double result = 0;
    float imageHalfHeight = [rouletteElements[0] getHeight] * 0.5;
    float radius = rouletteRect.size.width * DIAMETER_PERCENT_FOR_RADIUS;
    double angleFromImageHalfHeight = asin(imageHalfHeight/radius);
    if(CREATE_USING_MARGIN)
    {
        result = [self adjustAngleToCompleteCircle:(angleFromImageHalfHeight*2)];
    }
    else
    {
        result = angleFromImageHalfHeight * 2;
    }
    return result;
}

/**
 Funcion que ajusta el angulo al que se acomodaran las imagenes, distribuyendo el espacio restante
 entre los elementos de la ruleta a manera de margen
 
 @param angle Angulo que se ajustara
 @return Angulo resultante con el sobrante sumado a manera de margen
 */
-(double)adjustAngleToCompleteCircle:(double)angle
{
    double result;
    double onDegrees = [SFRouletteControllerBase toDegrees:angle];
    float elementsCount = (int)(360/onDegrees);
    double gap = 360 - (onDegrees * elementsCount);
    result = onDegrees + (gap / elementsCount);
    return [SFRouletteControllerBase toRadians:result];
}

/**
 Funcion que posiciona los elementos de la ruleta con respecto a un angulo
 
 @param angleForPosition Angulo al que se iran colocando los elementos de la ruleta
 */
-(void)settleOptions:(double)angleForPosition
{
    float initX = rouletteRect.size.width * DIAMETER_PERCENT_FOR_RADIUS;
    float initY = 0;
    float newX;
    float newY;
    float degrees = [SFRouletteControllerBase toDegrees:angleForPosition];
    float rotation;
    double currentAngle = angleForPosition * 0.5f;
    for(int i = 0;i < rouletteElements.count;i++)
    {
        newX = (cosf(currentAngle) * initX) - (sinf(currentAngle) * initY);
        newY = ((sinf(currentAngle) * initX) + (cosf(currentAngle) * initY));
        newX += rouletteRect.size.width * 0.5;
        newY += rouletteRect.size.height * 0.5;
        rotation = [SFRouletteControllerBase toDegrees:currentAngle];
        [rouletteElements[i] setPosition:newX yPosition:newY];
        [rouletteElements[i] setRotation:[SFRouletteControllerBase adjustAngleTo360Measure:rotation] angleRange:degrees];
        currentAngle += angleForPosition;
    }
    [controller setRotationLimits:360-(degrees*0.5f) bottomAngle:(360-(degrees*(rouletteElements.count))+(degrees*0.2f))];
}

/**
 Funcion que hace a un eemento candidato para poder ser seleccionado
 */
-(void)makeItCandidate:(BOOL)animated
{
    [self setCandidateFromRoulette];
    [self setCandidate:animated];
}

/**
 Funcion que asigna al candidato actual con respecto a la rotacion de la ruleta
 */
-(void)setCandidateFromRoulette
{
    float rouletteAngle = atan2(rouletteImage.transform.b,rouletteImage.transform.a);
    rouletteAngle = [SFRouletteControllerBase toDegrees:rouletteAngle];
    for(int i = 0;i < rouletteElements.count;i++)
    {
        if([rouletteElements[i] isAngleInRange:[SFRouletteControllerBase reflectAngle:rouletteAngle]])
        {
            currentCandidate = rouletteElements[i];
            break;
        }
    }
}

/**
 Funcion que informa al elemento de la ruleta que es el nuevo candidato
 */
-(void)setCandidate:(BOOL)animated
{
    [self setNewCandidate:animated];
    float angle;
    angle = [SFRouletteControllerBase adjustAngleTo360Measure:(-[self->currentCandidate getRotation])];
    [imagesController adjustRoulette:angle withAnimation:animated];
}

/**
 Función que remueve la cancidatura de las imagenes y la regresa al que corresponde.
 */
-(void)setNewCandidate:(BOOL)animated
{
    for(int i = 0;i < rouletteElements.count;i++)
    {
        if(rouletteElements[i] != currentCandidate)
        {
            [rouletteElements[i] hideCandidate];
        }
    }
    if(animated)
    {
        [currentCandidate setAsCandidate];
    }
}

/**
 Funcion que le quita la candidatura actual a un elemento de la ruleta
 */
-(void)removeCandidate
{
    [currentCandidate hideCandidate];
}

/**
 Funcion que evalua el inicio del evento touch de la ruleta
 
 @param position La posicion en la que inicio el evento touch
 @param currentTime El tiempo actual en el que se registra el evento touch
 */
-(void)onTouchStarted:(CGPoint)position timeStamp:(float)currentTime
{
    //[self removeCandidate];
    [super onTouchStarted:position timeStamp:currentTime];
}

/**
 Funcion que evalua el movimiento del evento touch
 
 @param touchPosition Posicion en la que se movio el evento touch
 */
-(void)onTouchMoved:(CGPoint)touchPosition  timeStamp:(float)currentTime
{
    float delta = 1;
    if(touchPosition.y <= previousTouch.y)
    {
        [controller rotateRoulette:-delta];
    }
    else
    {
        [controller rotateRoulette:delta];
    }
    [super onTouchMoved:touchPosition timeStamp:currentTime];
}

/**
 Funcion que maneja el gesto Tap detectado
 
 @param tapPosition Posicion en la que se registro un gesto Tap
 */
-(void)onTap:(CGPoint)tapPosition
{
    CGPoint selectionCenter = CGPointMake(screenWidth*VALUE_FOR_SELECTION_X_CENTER,screenHeight*VALUE_FOR_SELECTION_Y_CENTER);
    if([super didSelectElement:tapPosition areaCenter:selectionCenter])
    {
    }
    else
    {
        if(tapPosition.y < (screenHeight*0.5f))
        {
            [self advanceCandidate:-1 withAnimation:true];
        }
        else
        {
            [self advanceCandidate:1 withAnimation:true];
        }
        [self updateValue];
    }
}

-(void)moveSelection
{
    controller->allowInput = false;
    [currentCandidate candidateSelected:^()
    {
        if(selectionCallBack != nil)
        {
            controller->allowInput = true;
            selectionCallBack(currentCandidate.cityValue);
        }
    }];
}
    
/**
 Funcion que repoerta que se cambio el candidato
*/
-(void)updateValue
{
    if(valueChangedCallBack != nil)
    {
        valueChangedCallBack(currentCandidate.cityValue);
    }
}

/**
 Funcion que avance el candidato actual de entre los elementos de la ruleta
 
 @param elementsToAdvance La cantidad de elementos que avanzara
 */
-(void)advanceCandidate:(int)elementsToAdvance withAnimation:(BOOL)animated
{
    int index = (int)[rouletteElements indexOfObject:currentCandidate];
    if((index+elementsToAdvance) > -1 && (index+elementsToAdvance) < ((int)rouletteElements.count))
    {
        [self setCandidate:animated];
        [self removeCandidate];
        currentCandidate = rouletteElements[index+elementsToAdvance];
    }
    [self setCandidate:animated];
}

/**
 Funcion que mueve la ruleta al primer elemento de la ruleta y lo hace candidato para seleccion
 
 @return regresa el angulo al que se moverá la ruleta
 */
-(float)goToFirstCandidate:(BOOL)animated
{
    [self removeCandidate];
    currentCandidate = rouletteElements[0];
    [self setCandidate:animated];
    return (360 - [currentCandidate getRotation]);
}

/**
 Funcion que mueve la ruleta al ultimo elemento de la ruleta y lo hace candidato para seleccion
 
 @return regresa el angulo al que se moverá la ruleta
 */
-(float)goToLastCandidate:(BOOL)animated
{
    [self removeCandidate];
    currentCandidate = rouletteElements[rouletteElements.count-1];
    [self setCandidate:animated];
    return [currentCandidate getRotation];
}

-(void)releaseMemory
{
    for(int i = 0;i < rouletteElements.count;i++)
    {
        [rouletteElements[i] clearElement];
    }
    [rouletteElements removeAllObjects];
    controller = nil;
    imagesController = nil;
    rouletteElements = nil;
    currentCandidate = nil;
}

@end
