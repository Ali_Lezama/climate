//
//  RouletteElement.m
//  RouletteComponent
//
//  Created by Sferea on 05/01/18.
//  Copyright © 2018 Sferea. All rights reserved.
//

#import "SFImagesRouletteElement.h"
#import "SFImagesRouletteController.h"

static const float OPTIONS_PERCENT_HEIGHT = 0.827f;
static const float CANDIDATE_SIZE_RELATION_VERSUS_OPTION = 1.05f;
static const float CANDIDATE_HIDDEN_SCALE_PERCENT = 0.05f;
static const float CANDIDATE_SHOWN_SCALE_PERCENT = 20;
static const float IDDLE_SCALE_UP = 1.25f;
static const float IDDLE_SCALE_DOWN = 0.8f;
static const float IDDLE_ANIM_DURATION = 0.2f;
static const float IDDLE_TIME_FOR_ANIMATION = 2.0f;
static const float SCALE_ANIM_DURATION = 0.3f;
static const float SELECTED_ANIM_DURATION = 0.5f;
static const float ANGLE_RANGE_PRECENT_GAP = 0.2f;

/**
 Clase que funciona como elemento a agregar a la ruleta de imagenes
 */
@implementation SFImagesRouletteElement
{
    BOOL isCandidate;
    BOOL isHidding;
    BOOL selectionOnStack;
    float optionPercentSize;
    float candidatePercentSize;
    float rotation;
    float angleRange;
    float rouletteSize;
    CGRect optionRect;
    CGRect candidateRect;
    CGPoint previousCenter;
    UIImageView* option;
    UIImageView* candidate;
    
    NSString* temperatureValue;
    NSString* countryValue;
    NSString* latitudeValue;
    NSString* longitudeValue;
}

/**
 Constructor base del elemento de la ruleta de imagenes
 
 @param optionImage Imagen para el elemento de la ruleta cuando solo es opcion
 @param candidateImage Imagen para el elemento de la ruleta cuando solo es candidato
 */
-(id)init:(UIImageView*)optionImage candidateImg:(UIImageView*)candidateImage cityName:(NSString*)city
{
    self = [super init];
    if(self != nil)
    {
        option = optionImage;
        candidate = candidateImage;
        _cityValue = city;
    }
    return self;
}

/**
 Regresa la imagen de cuando el elemento es opcion
 
 @return Regresa la imagen de cuando es opcion
 */
-(UIImageView*)getOptionImage
{
    return option;
}

/**
 Regresa la imagen de cuando el elemento es candidato
 
 @return Regresa la imagen de cuando es candidato
 */
-(UIImageView*)getCandidateImage
{
    return candidate;
}

/**
 Regresa la rotcacion del elemento dentro de la ruleta
 
 @return La rotacion dentro de la ruleta
 */
-(float)getRotation
{
    return rotation + (angleRange * ANGLE_RANGE_PRECENT_GAP);
}

/**
 Regresa el ancho de la imagen cuando es opcion
 
 @return Regresa el ancho del elemento cuando es opcion
 */
-(float)getWidth
{
    return (optionRect.size.width * optionPercentSize);
}

/**
 Regresa la altura de la parte central hacia la ruleta
 
 @return regresa la altura de la parte dentral
 */
-(float)getHeight
{
    return ([option image].size.height * optionPercentSize * OPTIONS_PERCENT_HEIGHT);
}

/**
 Ajusta la escala del elemento con un porcentaje del alto de la ruleta que tiene que abarcar
 
 @param rouletteHeight La altura de la ruleta
 @param roulettePercent Porcentaje de la ruleta que debe de abarcar
 */
-(void)setScale:(float)rouletteHeight elementPercent:(float)roulettePercent
{
    rouletteSize = rouletteHeight;
    optionPercentSize = rouletteHeight*roulettePercent / [option image].size.height;
    candidatePercentSize = optionPercentSize * CANDIDATE_SIZE_RELATION_VERSUS_OPTION;
}

/**
 Asigna la posicion al elemento
 
 @param xPosition La nueva posicion en el eje X
 @param yPosition La nueva posicion en el eje Y
 */
-(void)setPosition:(float)xPosition yPosition:(float)yPosition
{
    optionRect = CGRectMake(xPosition,yPosition,0,0);
    optionRect.size.width = [option image].size.width * optionPercentSize;
    optionRect.size.height = [option image].size.height * optionPercentSize;
    optionRect.origin.x -= optionRect.size.width * 0.5f;
    optionRect.origin.y -= optionRect.size.height * 0.5f;
    [option setFrame:optionRect];
    candidateRect = CGRectMake(xPosition,yPosition,0,0);
    candidateRect.size.width = [candidate image].size.width * candidatePercentSize;
    candidateRect.size.height = [candidate image].size.height * candidatePercentSize;
    candidateRect.origin.x -= candidateRect.size.width * 0.5f;
    candidateRect.origin.y -= candidateRect.size.height * 0.5f;
    [candidate setFrame:candidateRect];
    candidate.transform = CGAffineTransformScale(candidate.transform,CANDIDATE_HIDDEN_SCALE_PERCENT,CANDIDATE_HIDDEN_SCALE_PERCENT);
    
    [candidate removeFromSuperview];
}

/**
 Asigna la rotacion al elemento
 
 @param newRotation La nueva rotacion del elemento
 @param range El rango de grados que abarca el elemento
 */
-(void)setRotation:(float)newRotation angleRange:(float)range
{
    rotation = newRotation;
    angleRange = range;
    option.transform = CGAffineTransformRotate(option.transform,[SFImagesRouletteController toRadians:rotation]);
    candidate.transform = CGAffineTransformRotate(candidate.transform,[SFImagesRouletteController toRadians:rotation]);
}

/**
 Funcion que evalua si un angulo esta dentro del angulo que abarca el elemento
 
 @param angle Angulo a evaluar
 @return Regresa true si el angulo esta dentro del rango
 */
-(BOOL)isAngleInRange:(float)angle
{
    BOOL result = false;
    angle -= (angleRange * ANGLE_RANGE_PRECENT_GAP);
    if(angle > (rotation-angleRange) && angle <= rotation)
    {
        result = true;
    }
    return result;
}

/**
 Cambia el estado del elemento a un modo donde se muestra la imagen de candidato
 */
-(void)setAsCandidate
{
    if(!isCandidate)
    {
        isCandidate = true;
        [option.superview addSubview:candidate];
        [UIView animateWithDuration:SCALE_ANIM_DURATION delay:(SCALE_ANIM_DURATION*0.5f) options:UIViewAnimationOptionCurveEaseOut animations:^{
            candidate.transform = CGAffineTransformScale(candidate.transform,CANDIDATE_SHOWN_SCALE_PERCENT,CANDIDATE_SHOWN_SCALE_PERCENT);
        } completion:nil];
        [self startIddleAnimation];
    }
}

/**
 Regresa al elemento a un modo donde muestra la imagen de cuando es opcion
 */
-(void)hideCandidate
{
    if(isCandidate)
    {
        isCandidate = false;
        [UIView animateWithDuration:(SCALE_ANIM_DURATION*0.5f) animations:^{
            isHidding = true;
            candidate.transform = CGAffineTransformScale(candidate.transform,CANDIDATE_HIDDEN_SCALE_PERCENT,CANDIDATE_HIDDEN_SCALE_PERCENT);
        } completion:^(BOOL finished){
            [candidate removeFromSuperview];
            isHidding = false;
            if(selectionOnStack)
            {
                [self candidateSelected:selectedAnimationCallback];
            }
        }];
    }
}

/**
 Funcion que inicia la animacion de desocupado
 */
-(void)startIddleAnimation
{
    if(isCandidate)
    {
        [UIView animateWithDuration:IDDLE_ANIM_DURATION delay:IDDLE_TIME_FOR_ANIMATION options:UIViewAnimationOptionCurveEaseIn animations:^(){
            candidate.transform = CGAffineTransformScale(candidate.transform,IDDLE_SCALE_UP,IDDLE_SCALE_UP);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:IDDLE_ANIM_DURATION animations:^(){
                candidate.transform = CGAffineTransformScale(candidate.transform,IDDLE_SCALE_DOWN,IDDLE_SCALE_DOWN);
            }];
        }];
    }
}

/**
 Funcion que crea la animacion de seleccion del candidato
 
 @param onCompleteCallBack funcion que se llamara cuando termine la animcacion
 */
-(void)candidateSelected:(void (^)(void))onCompleteCallBack
{
    selectedAnimationCallback = onCompleteCallBack;
    if(!isHidding)
    {
        [option.superview addSubview:candidate];
        //candidate.transform = CGAffineTransformScale(candidate.transform,CANDIDATE_SHOWN_SCALE_PERCENT,CANDIDATE_SHOWN_SCALE_PERCENT);
        [UIView animateWithDuration:SELECTED_ANIM_DURATION animations:^(){
            previousCenter = CGPointMake(candidate.center.x,candidate.center.y);
            CGPoint halfDistancePoint = CGPointMake((candidate.center.x-(rouletteSize * 0.5f)) * 0.5f,(candidate.center.y-(rouletteSize * 0.5f)) * 0.5f);
            candidate.center = CGPointMake((rouletteSize * 0.5f)+(halfDistancePoint.x),(rouletteSize * 0.5f)+(halfDistancePoint.y));
            candidate.transform = CGAffineTransformScale(candidate.transform,CANDIDATE_HIDDEN_SCALE_PERCENT,CANDIDATE_HIDDEN_SCALE_PERCENT);
            candidate.alpha = 0;
        } completion:^(BOOL finished) {
            selectionOnStack = false;
            selectedAnimationCallback();
            [self returnToRoulettePosition];
        }];
    }
    else
    {
        selectionOnStack = true;
    }
}

/**
 Funcion que regresa al candidato a su posicion original.
 */
-(void)returnToRoulettePosition
{
    [candidate removeFromSuperview];
    candidate.transform = CGAffineTransformScale(candidate.transform,CANDIDATE_SHOWN_SCALE_PERCENT,CANDIDATE_SHOWN_SCALE_PERCENT);
    candidate.center = CGPointMake(previousCenter.x,previousCenter.y);
    candidate.transform = CGAffineTransformScale(candidate.transform,CANDIDATE_HIDDEN_SCALE_PERCENT,CANDIDATE_HIDDEN_SCALE_PERCENT);
    candidate.alpha = 1;
}

-(void)clearElement
{
    [option removeFromSuperview];
    option = nil;
    [candidate removeFromSuperview];
    candidate = nil;
}

-(void)setData:(NSString*)country latitude:(NSString*)lat longitude:(NSString*)lon temperature:(NSString*)temp
{
    countryValue = country;
    latitudeValue = lat;
    longitudeValue = lon;
    temperatureValue = temp;
    
    [self addSelectedText];
}

-(void)addSelectedText
{
    [self addTextToOption:temperatureValue position:CGPointMake(0.16f,0.13f) size:CGPointMake(0.2f,0.25f)];
    [self addTextToOption:_cityValue position:CGPointMake(0.15f,0.16f) size:CGPointMake(0.2f,0.12f)];
    
    [self addTextToCandidate:temperatureValue position:CGPointMake(0.16f,0.13f) size:CGPointMake(0.2f,0.25f)];
    [self addTextToCandidate:_cityValue position:CGPointMake(0.18f,0.16f) size:CGPointMake(0.2f,0.12f)];
    [self addTextToCandidate:countryValue position:CGPointMake(0.13f,0.19f) size:CGPointMake(0.12f,0.1f)];
    [self addTextToCandidate:latitudeValue position:CGPointMake(0.12f,0.22f) size:CGPointMake(0.13f,0.1f)];
    [self addTextToCandidate:longitudeValue position:CGPointMake(0.11f,0.25f) size:CGPointMake(0.13f,0.1f)];
    
    CGRect imageRect = CGRectMake(0,0,100,100);
    imageRect.size.height = [UIImage imageNamed:@"sun"].size.height;
    imageRect.size.width = [UIImage imageNamed:@"sun"].size.width;
    UIImageView* candImg = [[UIImageView alloc] initWithFrame:imageRect];
    [candImg setImage:[UIImage imageNamed:@"sun"]];
    [candidate insertSubview:candImg atIndex:0];
    candImg.frame = CGRectMake(0,0,candidate.bounds.size.width*0.17f,candidate.bounds.size.width*0.17f);
    candImg.center = CGPointMake(candidate.bounds.size.width*0.22f,candidate.bounds.size.height*0.09f);
}

-(void)addTextToCandidate:(NSString*)text position:(CGPoint)pos size:(CGPoint)siz
{
    UILabel* label = [[UILabel alloc] init];
    [candidate insertSubview:label atIndex:0];
    [label setText:text];
    label.frame = CGRectMake(0,0,candidate.bounds.size.width*siz.x,candidate.bounds.size.height*siz.y);
    label.center = CGPointMake(candidate.bounds.size.width*pos.x,candidate.bounds.size.height*pos.y);
    label.adjustsFontSizeToFitWidth = true;
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor = [UIColor colorWithRed:0 green:131/255.0f blue:186/255.0f alpha:1.0];
}

-(void)addTextToOption:(NSString*)text position:(CGPoint)pos size:(CGPoint)siz
{
    UILabel* label = [[UILabel alloc] init];
    [option insertSubview:label atIndex:0];
    [label setText:text];
    label.frame = CGRectMake(0,0,candidate.bounds.size.width*siz.x,candidate.bounds.size.height*siz.y);
    label.center = CGPointMake(candidate.bounds.size.width*pos.x,candidate.bounds.size.height*pos.y);
    label.adjustsFontSizeToFitWidth = true;
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor = [UIColor colorWithRed:0 green:131/255.0f blue:186/255.0f alpha:0.6f];
}

@end
