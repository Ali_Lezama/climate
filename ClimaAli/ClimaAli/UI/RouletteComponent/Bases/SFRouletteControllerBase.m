//
//  RouletteControllerBase.m
//  RouletteComponent
//
//  Created by Sferea on 12/01/18.
//  Copyright © 2018 Sferea. All rights reserved.
//

#import "SFRouletteControllerBase.h"
#import "SFRouletteBase.h"

static const float MAXIMUM_SCREEN_WIDTH_PERCENT_FOR_TAP = 0.003f;
static const float MAXIMUM_TAP_GESTURE_TIME = 0.2f;
static const float MAXIMUM_SWIPE_GESTURE_TIME = 0.4f;
static const float MINIMUM_SCREEN_WIDTH_PERCENT_FOR_SWIPE = 0.004f;

/**
 Clase base para controlar una ruleta
 */
@implementation SFRouletteControllerBase

/**
 Constructor base que asigna la ruleta que se va a controlar
 
 @param toControl Ruleta que se va a controlar
 */
-(id)init:(SFRouletteBase *)toControl
{
    self = [super init];
    if(self != nil)
    {
        roulette = toControl;
        allowInput = true;
    }
    return self;
}

/**
 Asigna los valores para los limites de rotacion de la ruleta
 
 @param topAngleMaxValue Valor maximo del angulo de la ruleta en la parte de arriba de la ruleta
 @param bottomAngleMaxValue Valor maximo del angulo de la ruleta en la parte de abajo de la ruleta
 */
-(void)setRotationLimits:(float)topAngleMaxValue bottomAngle:(float)bottomAngleMaxValue;
{
    topMaxRotation = [SFRouletteControllerBase adjustAngleTo360Measure:topAngleMaxValue];
    bottomMaxRotation = [SFRouletteControllerBase adjustAngleTo360Measure:bottomAngleMaxValue];
    rotationRangeInverse = 1.0f / (topMaxRotation-bottomMaxRotation);
}

/**
 Cambia el valor de los angulos que se mueve la ruleta por cada actualizacion del movimiento del evento touch
 
 @param newValue Nuevo valor para el paso de la rotacion de la ruleta
 */
-(void)changeRotationStep:(float)newValue
{
    rotationStep = newValue;
}

/**
 Funcion que se encarga de rotar la ruleta
 
 @param rotationFactor Un factor por el que se multiplican los pasos de la rotacion, para generar el
                        nuevo angulo de la ruleta
 */
-(float)rotateRoulette:(float)rotationFactor
{
    float result = 0;
    if(allowInput)
    {
        imageAngle = rotationFactor * rotationStep;
        if(!useRotationLimits)
        {
            result = imageAngle;
            [self adjustRouletteImageAngle];
        }
        else
        {
            float angle = atan2([roulette getImage].transform.b,[roulette getImage].transform.a);
            angle = [SFRouletteControllerBase toDegrees:angle];
            angle = [SFRouletteControllerBase adjustAngleTo360Measure:angle];
            angle = (angle<=1) ? 360 : angle;
            angle += imageAngle;
            if(angle > bottomMaxRotation && angle < topMaxRotation)
            {
                result = imageAngle;
                [self adjustRouletteImageAngle];
            }
        }
    }
    return result;
}

/**
 Ajusta el angulo de la imagen de la ruleta a un angulo entre 0-360
 */
-(void)adjustRouletteImageAngle
{
    if(allowInput)
    {
        imageAngle = [SFRouletteControllerBase adjustAngleTo360Measure:imageAngle];
        [roulette getImage].transform = CGAffineTransformRotate([roulette getImage].transform,[SFRouletteControllerBase toRadians:imageAngle]);
    }
}

/**
 Funcion que se manda a llamar cuando se termina el evento touch
 
 @param position Posicion actual del evento touch
 @param previousTouch Posicion anterior del evento touch
 @param deltaTime El tiempo que duro el evento touch
 */
-(void)onTouchEnded:(CGPoint)position initPos:(CGPoint)previousTouch timeStamp:(float)deltaTime
{}

/**
 Funcion que evalua si el evento touch genero o no un gesto Tap
 
 @param squareDistance La distancia que recorrio el touch al cuadrado
 @param time Tiempo que duro el evento touch
 @return Regresa true si el evento genero o no un gesto Tap
 */
-(BOOL)isTapGesture:(float)squareDistance deltaTime:(float)time
{
    BOOL result = false;
    float maxDistance = roulette->screenWidth * MAXIMUM_SCREEN_WIDTH_PERCENT_FOR_TAP;
    maxDistance *= maxDistance;
    if(time < MAXIMUM_TAP_GESTURE_TIME && squareDistance < maxDistance && allowInput)
    {
        result = true;
    }
    return result;
}

/**
 Funcion que informa a la ruleta si se efectuo un Tap
 
 @param position Posicion en la que se genero el Tap
 */
-(void)onTapDetected:(CGPoint)position
{
    [roulette onTap:position];
}

/**
 Funcion que evalua si el evento touch genero o no un gesto swipe
 
 @param squareDistance La distancia que recorrio el evento touch al cuadrado
 @return Regresa true si el evento genero un gesto swipe
 */
-(BOOL)isSwipeGesture:(float)squareDistance deltaTime:(float)time
{
    BOOL result = false;
    float minDistance = roulette->screenWidth * MINIMUM_SCREEN_WIDTH_PERCENT_FOR_SWIPE;
    minDistance *= minDistance;
    if(time < MAXIMUM_SWIPE_GESTURE_TIME && squareDistance > minDistance && allowInput)
    {
        result = true;
    }
    return result;
}

/**
 Funcion que convierte un numero de grados a radianes
 
 @param degrees Valor del angulo a modificar
 @return Regresa el angulo recibido en Radianes
 */
+(CGFloat)toRadians:(CGFloat)degrees
{
    return degrees*(M_PI/180.0f);
}

/**
 Funcion que convierte un numero de radianes a grados
 
 @param radians Valor de un angulo en radianes
 @return Regresa el angulo recibido en Grados
 */
+(CGFloat)toDegrees:(CGFloat)radians
{
    return radians*(180.0f/M_PI);
}

/**
 Funcion que refleja un angulo sobre el eje X para que de el mismo angulo pero en su direccion contraria
 como si se evaluara con respecto a manecillas de un reloj
 
 @param angle Angulo que se reflejara
 @return Regresa el valor del angulo reflejado en el eje X
 */
+(float)reflectAngle:(float)angle
{
    float result  = [self adjustAngleTo360Measure:(-angle)];
    return result;
}

/**
 Ajusta el valor de un angulo a valores entre 0-360
 
 @param angle Angulos a modificar
 @return Regresa el valor del angulo entre 0-360
 */
+(float)adjustAngleTo360Measure:(float)angle
{
    float result = angle;
    if(result > 360)
    {
        result = result - 360;
    }
    else if(angle < 0)
    {
        result = 360 + result;
    }
    return result;
}
@end
