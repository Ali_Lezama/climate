//
//  SFRouletteBase.h
//  RouletteComponent
//
//  Created by Sferea on 12/01/18.
//  Copyright © 2018 Sferea. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "SFRouletteControllerBase.h"

typedef void(^RouletteSelectionCallBack)(NSString*);

extern const float ROULETTE_SCREEN_HEIGHT_PERCENT;
extern const float ROULETTE_X_PERCENT_POSITION;
extern const float ROULETTE_Y_PERCENT_POSITION;

@interface SFRouletteBase : NSObject
{
    @protected
        float touchTimer;
        float screenWidthPercentForSelectionWidth;
        CGPoint previousTouch;
        CGRect rouletteRect;
        UIImageView* rouletteImage;
        RouletteSelectionCallBack selectionCallBack;
        RouletteSelectionCallBack valueChangedCallBack;
    @public
        __strong SFRouletteControllerBase* controller;
        int screenWidth;
        int screenHeight;    
}

-(id)init;
-(void)showRoulette:(int)currentPosition;
-(void)hideRoulette;
-(UIImageView*)createImageWithName:(NSString*)name;
-(UIImageView*)getImage;
-(void)setScreenSize:(int)width screenHeight:(int)height;
-(void)setRouletteImage:(UIImageView*)source;
-(void)onTouchStarted:(CGPoint)position timeStamp:(float)currentTime;
-(void)onTouchMoved:(CGPoint)touchPosition timeStamp:(float)currentTime;
-(void)onTouchEnded:(CGPoint)position timeStamp:(float)currentTime elementSelected:(void(^)(NSString*))selection elementChanged:(void(^)(NSString*))changed;
-(void)onTouchEnded:(CGPoint)position timeStamp:(float)currentTime elementSelected:(void(^)(NSString*))selection;
-(void)onTap:(CGPoint)tapPosition;

@end
