//
//  RouletteControllerBase.h
//  RouletteComponent
//
//  Created by Sferea on 12/01/18.
//  Copyright © 2018 Sferea. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class SFRouletteBase;

@interface SFRouletteControllerBase : NSObject
{
    @protected
        float imageAngle;
        float topMaxRotation;
        float bottomMaxRotation;
        float rotationRangeInverse;
        float rotationStep;
        __weak SFRouletteBase* roulette;
    @public
        BOOL allowInput;
        BOOL useRotationLimits;
}

-(id)init:(SFRouletteBase*)toControl;
-(void)setRotationLimits:(float)topAngleMaxValue bottomAngle:(float)bottomAngleMaxValue;
-(void)changeRotationStep:(float)newValue;
-(float)rotateRoulette:(float)rotationFactor;
-(void)onTouchEnded:(CGPoint)position initPos:(CGPoint)previousTouch timeStamp:(float)deltaTime;

+(CGFloat)toRadians:(CGFloat)degrees;
+(CGFloat)toDegrees:(CGFloat)radians;
+(float)reflectAngle:(float)angle;
+(float)adjustAngleTo360Measure:(float)angle;
@end
