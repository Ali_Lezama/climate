//
//  SFRouletteBase.m
//  RouletteComponent
//
//  Created by Sferea on 12/01/18.
//  Copyright © 2018 Sferea. All rights reserved.
//

#import "SFRouletteBase.h"

const float ROULETTE_SCREEN_HEIGHT_PERCENT = 1.45f;
const float ROULETTE_X_PERCENT_POSITION = 0.85f;
const float ROULETTE_Y_PERCENT_POSITION = 0.43f;
const float SCREEN_WIDTH_PERCENT_FOR_HEIGHT = 1.5f;

/**
 Clase base para la ruletas
 */
@implementation SFRouletteBase

/**
 Constructor base de las ruletas
 */
-(id)init
{
    self = [super init];
    return self;
}

/**
 Funcion que muestra la ruleta en la pantalla
 */
-(void)showRoulette:(int)currentPosition
{
    rouletteImage.hidden = false;
}

/**
 Funcion que esconde la ruleta en la pantalla
 */
-(void)hideRoulette
{
    rouletteImage.hidden = true;
}

/**
 Funcion de apoyo para crear imagenes a partir de un nombre
 
 @param name Nombre del archivo con el que creara la nueva imagen
 @return Nueva imagen que se creo
 */
-(UIImageView*)createImageWithName:(NSString *)name
{
    CGRect imageRect = CGRectMake(0,0,100,100);
    imageRect.size.height = [UIImage imageNamed:name].size.height;
    imageRect.size.width = [UIImage imageNamed:name].size.width;
    UIImageView* result = [[UIImageView alloc] initWithFrame:imageRect];
    [result setImage:[UIImage imageNamed:name]];
    return result;
}

/**
 Acceso a la imagen de la ruleta
 
 @return Regresa el UIImageView donde se dibuja la ruleta
 */
-(UIImageView*)getImage
{
    return rouletteImage;
}

/**
 Registra el tamaño de la pantalla del dispositivo
 
 @param width Ancho de la pantalla
 @param height Alto de la pantalla
 */
-(void)setScreenSize:(int)width screenHeight:(int)height
{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    screenWidth = screenBound.size.width;
    screenHeight = screenBound.size.width * SCREEN_WIDTH_PERCENT_FOR_HEIGHT;
}

/**
 Funcion que registra la imagen de la ruleta
 
 @param source Un UIImageView fuente del cual se copiara la ruleta
 */
-(void)setRouletteImage:(UIImageView *)source
{
    rouletteImage = source;
    [self initRouletteToScreenWidthSquare];
}

/**
 Inicializa el objeto view de la ruleta a un cuadrado del ancho de la pantalla
 */
-(void)initRouletteToScreenWidthSquare
{
    rouletteRect = CGRectMake(0,0,screenWidth,screenWidth);
    [rouletteImage setFrame:rouletteRect];
}

/**
 Ajusta la ruleta con los valores de configuracion de la parte superior de la clase
 */
-(void)adjustRouletteToConfiguration
{
    float areaPercent = 1;
    float xPosition = 0;
    float yPosition = ((screenHeight - rouletteRect.size.width) * ROULETTE_Y_PERCENT_POSITION);
    BOOL showSmallRoulette = false;
    if(!showSmallRoulette)
    {
        areaPercent = (screenHeight * ROULETTE_SCREEN_HEIGHT_PERCENT) / (rouletteRect.size.width);
        xPosition = -(rouletteRect.size.width * ROULETTE_X_PERCENT_POSITION);
    }
    [self moveRouletteTo:xPosition yPosition:yPosition];
    rouletteImage.transform = CGAffineTransformScale(rouletteImage.transform,areaPercent,areaPercent);
}

/**
 Mueve la ruleta a una posicion X y Y
 
 @param targetXPosition posicion objetivo en el eje X de la ruleta
 @param targetYPosition posicion objetivo en el eje Y de la ruleta
 */
-(void)moveRouletteTo:(float)targetXPosition yPosition:(float)targetYPosition
{
    rouletteRect.origin.x = targetXPosition;
    rouletteRect.origin.y = targetYPosition;
    [rouletteImage setFrame:rouletteRect];
}

/**
Funcion que recibe el inicio de un evento touch
 
 @param position La posicion en la que se registro el touch
 @param currentTime El tiempo actual en el que se registro el touch
 */
-(void)onTouchStarted:(CGPoint)position timeStamp:(float)currentTime
{
    previousTouch = position;
    touchTimer = currentTime;
}

/**
 Funcion que recibe un movimiento del evento touch
 
 @param touchPosition La posicion actual del evento touch
 */
-(void)onTouchMoved:(CGPoint)touchPosition timeStamp:(float)currentTime
{
    previousTouch = touchPosition;
}

/**
 Funcion que recibe el final de un evento touch
 
 @param position Posicion en la que se registro el evento touch
 @param currentTime El tiempo actual en el que se recibe el final del evento touch
 @param selection Un Bloque que servira a manera de callBack para cuando se elige un elemento
 @param changed Un Bloque que servira a manera de callBack para cuando se cambia un elemento
 */
-(void)onTouchEnded:(CGPoint)position timeStamp:(float)currentTime elementSelected:(void(^)(NSString*))selection elementChanged:(void(^)(NSString*))changed
{
    valueChangedCallBack = changed;
    [self onTouchEnded:position timeStamp:currentTime elementSelected:selection];
}

/**
 Funcion que recibe el final de un evento touch
 
 @param position Posicion en la que se registro el evento touch
 @param currentTime El tiempo actual en el que se recibe el final del evento touch
 @param selection Un Bloque que servira a manera de callBack para cuando se elige un elemento
 */
-(void)onTouchEnded:(CGPoint)position timeStamp:(float)currentTime elementSelected:(void(^)(NSString*))selection
{
    float deltaTime = currentTime - touchTimer;
    selectionCallBack = selection;
    [controller onTouchEnded:position initPos:previousTouch timeStamp:deltaTime];
}

/**
 Funcion que se manda a llamar si el controlador de la ruleta detecta un Tap en la pantalla
 
 @param tapPosition La posicion del Tap
 */
-(void)onTap:(CGPoint)tapPosition
{}

/**
 Evalua sin una posicion esta dentro del area para elegir un elemento de la pantalla.
 Se usa un punto como centro del area de seleccion
 
 @param toEvaluate Posicion que se va a evaluar
 @param areaPosition Posicion del centro del area
 @return Regresa true si el primer punto esta dentro del area
 */
-(BOOL)didSelectElement:(CGPoint)toEvaluate areaCenter:(CGPoint)areaPosition
{
    BOOL result = false;
    float halfSize = screenWidth * screenWidthPercentForSelectionWidth * 0.5f;
    float leftLimit = areaPosition.x - (halfSize * 2);
    float topLimit = areaPosition.y - halfSize;
    float rightLimit = areaPosition.x + (halfSize * 2);
    float botomLimit = areaPosition.y + halfSize;
    
    if(toEvaluate.x > leftLimit && toEvaluate.x < rightLimit &&
       toEvaluate.y > topLimit && toEvaluate.y < botomLimit)
    {
        result = true;
    }
    return result;
}
@end
