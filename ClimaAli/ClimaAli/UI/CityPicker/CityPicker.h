//
//  CityPicker.h
//  ClimaAli
//
//  Created by Sferea Book on 09/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface CityPicker : UIViewController <UIPickerViewDelegate>

@property (weak,nonatomic) ViewController* previousView;

@property (weak, nonatomic) IBOutlet UIView *background;
@property (weak, nonatomic) IBOutlet UIView *header;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
@property (weak, nonatomic) IBOutlet UIPickerView *citiesMenu;

@end
