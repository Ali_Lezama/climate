//
//  CityPicker.m
//  ClimaAli
//
//  Created by Sferea Book on 09/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//

#import "CityPicker.h"
#import "AppDelegate.h"
#import "City+CoreDataClass.h"

@interface CityPicker ()

@end

@implementation CityPicker
{
    NSString* selection;
    NSArray<City*>* allData;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self adjustUI];
    
    [self readDataBase];
    
    _citiesMenu.delegate = self;
    _citiesMenu.dataSource = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)readDataBase
{
    NSPersistentContainer* persistentContainer = [(AppDelegate*)[[UIApplication sharedApplication] delegate] persistentContainer];
    allData = [City fetchOrderedCountries:persistentContainer.viewContext];
    selection = allData[0].name;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView*)picker numberOfRowsInComponent:(NSInteger)component
{
    return allData.count;
}

-(NSString*)pickerView:(UIPickerView*)picker titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"%@",allData[row].name];
}

-(void)pickerView:(UIPickerView*)picker didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    selection = allData[row].name;
    NSLog(@"Selected:%@",selection);
}

- (IBAction)onCancel:(id)sender
{
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)onAccept:(id)sender
{
    [_previousView saveCityOnPreferences:selection];
    [self dismissViewControllerAnimated:true completion:nil];
}

-(void)adjustUI
{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGFloat wid = screenBound.size.width;
    CGFloat hei = screenBound.size.height;
    self.background.bounds = CGRectMake(0,0,wid*0.75f,hei*0.3f);
    self.background.center = CGPointMake(wid*0.5f,hei*0.88f);
    self.background.layer.cornerRadius = 15;
    self.background.layer.masksToBounds = NO;
    self.background.layer.shadowRadius = 1.0;
    self.background.layer.shadowColor = [UIColor colorWithRed:129/255.0f green:207/255.0f blue:224/255.0f alpha:1.0].CGColor;
    self.background.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    self.background.layer.shadowOpacity = 1.0;
    
    self.header.bounds = CGRectMake(0,0,wid*0.75f,hei*0.08f);
    self.header.center = CGPointMake(wid*0.5f,hei*0.77f);
    self.header.layer.cornerRadius = 15;
    self.header.backgroundColor = [UIColor colorWithRed:129/255.0f green:207/255.0f blue:224/255.0f alpha:1.0];
    
    self.cancelButton.bounds = CGRectMake(0,0,wid*0.2f,hei*0.07f);
    self.cancelButton.center = CGPointMake(wid*0.25f,hei*0.77f);
    
    self.acceptButton.bounds = CGRectMake(0,0,wid*0.2f,hei*0.07f);
    self.acceptButton.center = CGPointMake(wid*0.75f,hei*0.77f);
    
    self.citiesMenu.center = CGPointMake(wid*0.5f,hei*0.9f);
    self.citiesMenu.bounds = CGRectMake(0,0,wid*0.7f,hei*0.2f);
}

@end
