//
//  AppDelegate.h
//  ClimaAli
//
//  Created by Sferea Book on 08/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate> 

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

-(void)saveContext;

@end

