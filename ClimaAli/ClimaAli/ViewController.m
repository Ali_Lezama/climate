//
//  ViewController.m
//  ClimaAli
//
//  Created by Sferea Book on 08/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "City+CoreDataClass.h"
#import "ClimateAPI.h"
#import "CityPicker.h"
#import "CityRegistration.h"
#import "WebManager.h"

static const NSString* PREFERED_CITIES = @"userCities";
static const NSString* CITIES_MENU_SEGUE = @"showCitiesMenu";
static const NSString* CITIES_REGISTRY_SEGUE = @"showCityRegistry";

@interface ViewController ()

@end

@implementation ViewController
{
    NSArray<City*>* allData;
    NSMutableArray<NSString*>* preferedCities;
    NSMutableArray<ClimateDTO*>* climatesResponse;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self adjustUI];
    
    [self readDataBase];
    [self loadPreferences];
    [self requestForClimates];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:CITIES_MENU_SEGUE])
    {
        CityPicker* controller = (CityPicker*)segue.destinationViewController;
        controller.previousView = self;
    }
    else if([segue.identifier isEqualToString:CITIES_REGISTRY_SEGUE])
    {
        CityRegistration* controller = (CityRegistration*)segue.destinationViewController;
        controller.previousView = self;
    }
}

-(void)loadPreferences
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray* savedCities = [userDefaults arrayForKey:PREFERED_CITIES];
    if(savedCities != nil)
    {
        preferedCities = [[NSMutableArray<NSString*> alloc] initWithArray:savedCities];
    }
    else
    {
        preferedCities = [[NSMutableArray<NSString*> alloc] init];
        [self saveCityOnPreferences:allData[1].name];
    }
}

-(void)saveCityOnPreferences:(NSString*)city
{
    if(![preferedCities containsObject:city])
    {
        [preferedCities addObject:city];
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:preferedCities forKey:PREFERED_CITIES];
        [userDefaults synchronize];
        
        [self createRoulette];
        [self requestForClimates];
    }
    else
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"The city you try to add is already on your list." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(void)deleteCityOnPreferences:(NSString*)city
{
    if([preferedCities containsObject:city] && preferedCities.count > 1)
    {
        [preferedCities removeObject:city];
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:preferedCities forKey:PREFERED_CITIES];
        [userDefaults synchronize];
        
        [self createRoulette];
        [self requestForClimates];
    }
}

- (IBAction)onDeleteSelected:(id)sender
{
    if(preferedCities.count > 1)
    {
        [self.rouletteManager moveSelection];
    }
    else
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"You must always have at least one city." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(void)readDataBase
{
    NSPersistentContainer* persistentContainer = [(AppDelegate*)[[UIApplication sharedApplication] delegate] persistentContainer];
    allData = [City fetchOrderedCountries:persistentContainer.viewContext];
}

-(void)requestForClimates
{
    if([WebManager hasNetwork])
    {
        if([allData count] != 0)
        {
            climatesResponse = [[NSMutableArray<ClimateDTO*> alloc] init];
            for(int i = 0;i < preferedCities.count;i++)
            {
                City* city = [self getCityWithName:preferedCities[i]];
                [climatesResponse addObject:[ClimateAPI makeClimateRequest:city.longitude latitude:city.latitude]];
            }
            [self updateCoreDataClimates];
        }
    }
    else
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Theres no Internet on the app. The data shown will not be updated." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(void)debugClimates
{
    for(int i = 0;i < climatesResponse.count;i++)
    {
        NSLog(@"Temp:%@",climatesResponse[i].temperature);
        NSLog(@"Icon:%@",climatesResponse[i].iconURL);
        NSLog(@"Date:%@",climatesResponse[i].dateUpdate);
    }
}

-(void)updateCoreDataClimates
{
    for(int i = 0;i < preferedCities.count;i++)
    {
        [self getCityWithName:preferedCities[i]].lastClimateRegistry = climatesResponse[i].temperature;
    }
    [(AppDelegate*)[[UIApplication sharedApplication] delegate] saveContext];
}


-(City*)getCityWithName:(NSString*)city
{
    NSPersistentContainer* persistentContainer = [(AppDelegate*)[[UIApplication sharedApplication] delegate] persistentContainer];
    allData = [City fetchOrderedCountries:persistentContainer.viewContext];
    
    City* result = nil;
    
    if([allData count] != 0)
    {
        for(int j = 0;j < allData.count;j++)
        {
            if([allData[j].name isEqualToString:city])
            {
                result = allData[j];
                break;
            }
        }
    }
    return result;
}

-(void)registerNewCity:(CityData*)data
{
    NSPersistentContainer* persistentContainer = [(AppDelegate*)[[UIApplication sharedApplication] delegate] persistentContainer];
    allData = [City fetchOrderedCountries:persistentContainer.viewContext];
    
    if([allData count] != 0)
    {
        if(![City cityRegistered:allData city:data.name] && ![City coordinatesRegistered:allData longitude:data.longitude latitude:data.latitude])
        {
            City* city = [City registerCityWithContext:persistentContainer.viewContext];
            city.name = data.name;
            city.country = data.country;
            city.latitude = data.latitude;
            city.longitude = data.longitude;
            [(AppDelegate*)[[UIApplication sharedApplication] delegate] saveContext];
        }
        else
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"The city you try to register is already on the system. Please check name and coordinates." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
}

-(void)adjustUI
{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGFloat wid = screenBound.size.width;
    CGFloat hei = screenBound.size.height;
    self.rouletteArea.bounds = screenBound;
    
    
    self.eliminateButton.bounds = CGRectMake(0,0,wid*0.2f,hei*0.2f);
    self.eliminateButton.center = CGPointMake(wid*0.06f,hei*0.37f);
    self.eliminateImg.bounds = CGRectMake(0,0,wid*0.2f,hei*0.2f);
    self.eliminateImg.center = CGPointMake(wid*0.06f,hei*0.37f);
    
    self.cityPicker.bounds = CGRectMake(0,0,wid*0.95f,hei*0.1f);
    self.cityPicker.center = CGPointMake(wid*0.71f,hei*0.77f);
    self.pickerImg.bounds = CGRectMake(0,0,wid*0.95f,hei*0.7f);
    self.pickerImg.center = CGPointMake(wid*0.71f,hei*0.77f);
    
    self.cityRegistry.bounds = CGRectMake(0,0,wid*0.95f,hei*0.1f);
    self.cityRegistry.center = CGPointMake(wid*0.71f,hei*0.89f);
    self.registryImg.bounds = CGRectMake(0,0,wid*0.95f,hei*0.7f);
    self.registryImg.center = CGPointMake(wid*0.71f,hei*0.89f);
}

-(void)viewDidLayoutSubviews
{
    [self createRoulette];
}

-(void)createRoulette
{
    if(self.rouletteManager)
    {
        [self.rouletteManager releaseMemory];
        self.rouletteManager = nil;
    }
    dispatch_async(dispatch_get_main_queue(), ^
    {
        if (!self.rouletteManager)
        {
            [self initializeVaribles];
            [self generateRoulette];
            [self.rouletteManager showRoulette:0];
            [self registerForRouletteEvents:CGPointZero timeStamp:0];
        }
    });
}

-(void)initializeVaribles
{
    self.rouletteManager = [[SFImagesRouletteManager alloc] init];
    self.rouletteData = [[NSMutableArray<SFImagesRouletteElement*> alloc] init];
    [self setRoulette];
}

-(void)setRoulette
{
    [self.rouletteManager setScreenSize:CGRectGetWidth(self.rouletteArea.bounds) screenHeight:CGRectGetHeight(self.rouletteArea.bounds)];
    [self.rouletteManager setRouletteImage:[self.rouletteManager createImageWithName:@"Roulette.png"]];
    [self.rouletteArea addSubview:self.rouletteManager.getImage];
}

-(void)generateRoulette
{
    [self createData];
    [self.rouletteManager assignRouletteContent:self.rouletteData];
}

-(void)createData
{
    [self.rouletteData removeAllObjects];
    
    for(int i = 0;i < preferedCities.count;i++)
    {
        [self.rouletteData addObject:[[SFImagesRouletteElement alloc] init:
                                       [self.rouletteManager createImageWithName:@"cloudOff"]
                            candidateImg:[self.rouletteManager createImageWithName:@"cloudOn"]
                            cityName:preferedCities[i]]];
        
        City* city = [self getCityWithName:preferedCities[i]];
        [self.rouletteData[i] setData:city.country latitude:city.latitude longitude:city.longitude temperature:city.lastClimateRegistry];
    }
}

-(void)touchesBegan:(NSSet<UITouch*>*)touches withEvent:(UIEvent*)event
{
    UITouch* touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:touch.view];
    if([touch.view isEqual:self.rouletteArea] || touch.view == nil)
    {
        [self.rouletteManager onTouchStarted:touchLocation timeStamp:touch.timestamp];
        return;
    }
}

-(void)touchesMoved:(NSSet<UITouch*>*)touches withEvent:(UIEvent*)event
{
    UITouch* touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:touch.view];
    if([touch.view isEqual:self.rouletteArea])
    {
        [self.rouletteManager onTouchMoved:touchLocation timeStamp:touch.timestamp];
        return;
    }
}

-(void)touchesEnded:(NSSet<UITouch*>*)touches withEvent:(UIEvent*)event
{
    UITouch* touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:touch.view];
    if([touch.view isEqual:self.rouletteArea])
    {
        [self registerForRouletteEvents:touchLocation timeStamp:touch.timestamp];
        return;
    }
}

-(void)registerForRouletteEvents:(CGPoint)position timeStamp:(float)currentTime
{
    [self.rouletteManager onTouchEnded:position timeStamp:currentTime
    elementSelected:^(NSString* a)
    {
         NSLog(@"Selecciono:%@",a);
         
        [self deleteCityOnPreferences:a];
    }
    elementChanged:^(NSString* a)
    {
    }];
}
@end
