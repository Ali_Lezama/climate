//
//  AppDelegate.m
//  ClimaAli
//
//  Created by Sferea Book on 08/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//

#import "AppDelegate.h"
#import "RawData.h"
#import "City+CoreDataClass.h"
#import "CityData.h"
#import "ClimateAPI.h"
#import "WebManager.h"

static const NSString* PREFERED_CITIES = @"userCities";
static const NSString* NOTIFICATION_ID = @"notificationID";

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert) completionHandler:^(BOOL granted, NSError * _Nullable error)
     {
         if (!error)
         {
             NSLog(@"request authorization succeeded!");
         }
     }];
    
    [self cancelNotifications:application];
    [self initDataBase];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    [self saveContext];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self createNotification];
    [self saveContext];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self cancelNotifications:application];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [self cancelNotifications:application];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [self createNotification];
    [self saveContext];
}

-(void)initDataBase
{
    NSArray<City*>* allData = [City fetchOrderedCountries:self.persistentContainer.viewContext];
    
    if([allData count] == 0)
    {
        RawData* rawData = [[RawData alloc] init];
        NSArray<CityData*>* hardCodedData = [rawData getRawData];
        
        for(int i = 0;i < hardCodedData.count;i++)
        {
            City* city = [City registerCityWithContext:self.persistentContainer.viewContext];
            city.name = hardCodedData[i].name;
            city.country = hardCodedData[i].country;
            city.latitude = hardCodedData[i].latitude;
            city.longitude = hardCodedData[i].longitude;
            city.lastClimateRegistry = @"20c";
        }
        [self saveContext];
    }
    else
    {
        for(int i = 0;i < allData.count;i++)
        {
            NSLog(@"*********************************");
            NSLog(@"Name:%@",allData[i].name);
            NSLog(@"Country:%@",allData[i].country);
            NSLog(@"Latitude:%@",allData[i].latitude);
            NSLog(@"Longitude:%@",allData[i].longitude);
            NSLog(@"Climate:%@",allData[i].lastClimateRegistry);
            NSLog(@"---------------------------------");
        }
    }
}

-(void)createNotification
{
    NSArray<City*>* allData = [City fetchOrderedCountries:self.persistentContainer.viewContext];
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray* savedCities = [userDefaults arrayForKey:PREFERED_CITIES];
    NSString* climate = @"";
    for(int i = 0;i < allData.count;i++)
    {
        if([allData[i].name isEqualToString:savedCities[0]])
        {
            if([WebManager hasNetwork])
            {
                ClimateDTO* response = [ClimateAPI makeClimateRequest:allData[i].longitude latitude:allData[i].latitude];
                climate = response.temperature;
            }
            else
            {
                climate = allData[i].lastClimateRegistry;
            }
            break;
        }
     }
    if(savedCities != nil)
    {
        UNMutableNotificationContent *objNotificationContent = [[UNMutableNotificationContent alloc] init];
        objNotificationContent.title = [NSString localizedUserNotificationStringForKey:@"The climate report is..." arguments:nil];
        objNotificationContent.body = [NSString localizedUserNotificationStringForKey:[NSString stringWithFormat:@"The last climate registried on %@ was %@",savedCities[0],climate] arguments:nil];
        objNotificationContent.sound = [UNNotificationSound defaultSound];
        objNotificationContent.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
        UNTimeIntervalNotificationTrigger* trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:60*60*2 repeats:true];

        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:NOTIFICATION_ID content:objNotificationContent trigger:trigger];
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error)
         {
             if (!error)
             {
                 NSLog(@"Local Notification succeeded");
             }
             else
             {
                 NSLog(@"Local Notification failed");
             }
         }];
    }
}

-(void)cancelNotifications:(UIApplication*)application
{
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center removeAllPendingNotificationRequests];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer
{
    @synchronized (self)
    {
        if (_persistentContainer == nil)
        {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"ClimaAli"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error)
            {
                if (error != nil)
                {
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext
{
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
