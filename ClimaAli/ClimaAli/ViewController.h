//
//  ViewController.h
//  ClimaAli
//
//  Created by Sferea Book on 08/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CityData.h"
#import "SFImagesRouletteManager.h"

@interface ViewController : UIViewController

@property SFImagesRouletteManager* rouletteManager;
@property NSMutableArray<SFImagesRouletteElement*>* rouletteData;

@property (weak, nonatomic) IBOutlet UIButton *eliminateButton;
@property (weak, nonatomic) IBOutlet UIButton *cityPicker;
@property (weak, nonatomic) IBOutlet UIButton *cityRegistry;
@property (weak, nonatomic) IBOutlet UIView *rouletteArea;
@property (weak, nonatomic) IBOutlet UIImageView *eliminateImg;
@property (weak, nonatomic) IBOutlet UIImageView *pickerImg;
@property (weak, nonatomic) IBOutlet UIImageView *registryImg;

-(void)saveCityOnPreferences:(NSString*)city;
-(void)registerNewCity:(CityData*)data;

@end

