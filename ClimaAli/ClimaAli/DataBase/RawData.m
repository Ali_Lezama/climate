//
//  RawData.m
//  ClimaAli
//
//  Created by Sferea Book on 09/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//

#import "RawData.h"
#import "CityData.h"

@implementation RawData
{
    NSArray* names;
    NSArray* countries;
    NSArray* latitudes;
    NSArray* longitudes;
}


-(id)init
{
    self = [super init];
    if(self != nil)
    {
        names = @[@"Mexico",@"Guadalajara",@"Puebla",@"Ecatepec",@"Puebla de Zaragoza",@"Tijuana",@"Monterrey",@"Leon",@"Zapopan",@"Mérida"];
        countries = @[@"Mexico",@"Mexico",@"Mexico",@"Mexico",@"Mexico",@"Mexico",@"Mexico",@"Mexico",@"Mexico",@"Mexico"];
        latitudes = @[@"19.4284706",@"20.6668205",@"19.0379295",@"19.6049194",@"19.0379295",@"32.5027008",@"25.6750698",@"21.1290798",@"20.7235603",@"20.9753704"];
        longitudes = @[@"-99.1276627",@"-103.3918228",@"-98.2034607",@"-99.0606384",@"-98.2034607",@"-117.0037079",@"-100.3184662",@"-101.6737366",@"-103.3847885",@"-89.6169586"];
    }
    return self;
}

-(NSArray*)getRawData
{
    NSMutableArray<CityData*>* data = [[NSMutableArray<CityData*> alloc] init];
    
    for(int i = 0;i < names.count;i++)
    {
        CityData* city = [[CityData alloc] initWithData:names[i] country:countries[i] longitude:longitudes[i] latitude:latitudes[i]];
        [data addObject:city];
    }
        
    return data;
}

@end
