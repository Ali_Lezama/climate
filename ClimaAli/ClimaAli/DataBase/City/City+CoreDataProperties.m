//
//  City+CoreDataProperties.m
//  ClimaAli
//
//  Created by Sferea Book on 09/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//
//

#import "City+CoreDataProperties.h"

@implementation City (CoreDataProperties)

+ (NSFetchRequest<City *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"City"];
}

@dynamic country;
@dynamic lastClimateRegistry;
@dynamic latitude;
@dynamic longitude;
@dynamic name;

@end
