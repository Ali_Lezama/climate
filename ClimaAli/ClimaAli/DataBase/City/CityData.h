//
//  CityData.h
//  ClimaAli
//
//  Created by Sferea Book on 09/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityData : NSObject

@property (nullable, nonatomic) NSString *country;
@property (nullable, nonatomic) NSString *lastClimateRegistry;
@property (nullable, nonatomic) NSString *latitude;
@property (nullable, nonatomic) NSString *longitude;
@property (nullable, nonatomic) NSString *name;

-(id)initWithData:(NSString*)nam country:(NSString*)con longitude:(NSString*)lon latitude:(NSString*)lat;

@end
