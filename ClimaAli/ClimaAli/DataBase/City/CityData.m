//
//  CityData.m
//  ClimaAli
//
//  Created by Sferea Book on 09/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//

#import "CityData.h"

@implementation CityData

-(id)initWithData:(NSString*)nam country:(NSString*)con longitude:(NSString*)lon latitude:(NSString*)lat
{
    self = [super init];
    if(self != nil)
    {
        self.name = nam;
        self.country = con;
        self.longitude = lon;
        self.latitude = lat;
    }
    return self;
}

@end
