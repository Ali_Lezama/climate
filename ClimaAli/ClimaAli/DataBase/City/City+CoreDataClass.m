//
//  City+CoreDataClass.m
//  ClimaAli
//
//  Created by Sferea Book on 09/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//
//

#import "City+CoreDataClass.h"

@implementation City

+(id)registerCityWithContext:(NSManagedObjectContext*)context
{
    City* city = [NSEntityDescription insertNewObjectForEntityForName:@"City" inManagedObjectContext:context];
    return city;
}

+(NSArray<City*>*)fetchOrderedCountries:(NSManagedObjectContext*)context
{
    NSEntityDescription* entityDescription = [NSEntityDescription entityForName:@"City" inManagedObjectContext:context];
    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:true];
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entityDescription];
    [request setSortDescriptors:@[sortDescriptor]];
    
    NSError* error;
    NSArray<City*>* result = [context executeFetchRequest:request error:&error];
    return result;
}

+(BOOL)coordinatesRegistered:(NSArray<City*>*)data longitude:(NSString*)lon latitude:(NSString*)lan
{
    BOOL result = false;
    
    for(int i = 0;i < data.count;i++)
    {
        if([data[i].latitude isEqualToString:lan] && [data[i].longitude isEqualToString:lon])
        {
            result = true;
            break;
        }
    }
    NSInteger latitude = [lan integerValue];
    if(latitude < -90 || latitude > 90)
    {
        result = false;
    }
    NSInteger longitude = [lon integerValue];
    if(longitude < -180 || longitude > 180)
    {
        result = false;
    }
    
    return result;
}

+(BOOL)cityRegistered:(NSArray<City*>*)data city:(NSString*)name
{
    BOOL result = false;
    
    for(int i = 0;i < data.count;i++)
    {
        if([data[i].name isEqualToString:name])
        {
            result = true;
            break;
        }
    }
    
    return result;
}

@end
