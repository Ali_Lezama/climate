//
//  City+CoreDataClass.h
//  ClimaAli
//
//  Created by Sferea Book on 09/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface City : NSManagedObject

+(id)registerCityWithContext:(NSManagedObjectContext*)context;
+(NSArray<City*>*)fetchOrderedCountries:(NSManagedObjectContext*)context;
+(BOOL)coordinatesRegistered:(NSArray<City*>*)data longitude:(NSString*)lon latitude:(NSString*)lan;
+(BOOL)cityRegistered:(NSArray<City*>*)data city:(NSString*)name;

@end

NS_ASSUME_NONNULL_END

#import "City+CoreDataProperties.h"
