//
//  City+CoreDataProperties.h
//  ClimaAli
//
//  Created by Sferea Book on 09/02/19.
//  Copyright © 2019 aliLezama. All rights reserved.
//
//

#import "City+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface City (CoreDataProperties)

+ (NSFetchRequest<City *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *country;
@property (nullable, nonatomic, copy) NSString *lastClimateRegistry;
@property (nullable, nonatomic, copy) NSString *latitude;
@property (nullable, nonatomic, copy) NSString *longitude;
@property (nullable, nonatomic, copy) NSString *name;

@end

NS_ASSUME_NONNULL_END
